#!/usr/bin/python
import ROOT
from root_numpy import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cPickle as pickle
import mglearn
import sys
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split #splits the training from the testing                       
from sklearn.model_selection import cross_val_score

infile = '/eos/user/a/anmrodri/Tevatron/HIP/PG/monopole_single_1gD_500GeV.MonFile.root'
#infile2 = '/eos/user/a/anmrodri/Tevatron/HIP/PG/monopole_single_2gD_500GeV.MonFile.root'
infile2 = '/eos/user/a/anmrodri/Tevatron/HIP/Data2015+16/data15.root'
tree1 = 'MonAnaTree'
tree2 = 'MonAnaTree'
#columns = list_branches(infile, tree1)
columns = ['nTRT4','nHTTRT4','TotalEnergyinPre','TotalEnergyinEM1','TotalEnergyinEM2']
#columns = ['monMomentum','monPhi','monEta','monEkin']
#names = ['monMomentum','monPhi','monEta','monEkin']
names = columns
selection = ''

def main(infile, tree1, infile2, tree2):

    outfile = '{}.p'.format(infile[:-5])

    tree1 = ROOT.TChain(tree1)
    tree1.Add(infile)
    tree2 = ROOT.TChain(tree2)
    tree2.Add(infile2)

    print 'Got TTrees, reading into array'
    arr1 = np.array(tree2array(tree1, branches=columns, selection=selection)[:10000])
    arr2 = np.array(tree2array(tree2, branches=columns, selection=selection)[:10000])

    names.append('labels')
    print 'Tree2array finished, starting array fixing.'

#    for index, row in enumerate(arr1):
#        for element in row:
#            print element
#            if isinstance(element, (list, tuple, np.ndarray)) and element.size == 0:
#                print "*******COUNT:",index
#                print "**MOCOS1**"
#            if isinstance(element, (list, tuple, np.ndarray)):
#                print element

    arr1 = np.array([[0 if isinstance(element, (list, tuple, np.ndarray)) and element.size == 0 else element for element in row] for row in arr1])    
    arr1 = np.array([[element[0] if isinstance(element, (list, tuple, np.ndarray)) else element for element in row] for row in arr1])
    arr2 = np.array([[0 if isinstance(element, (list, tuple, np.ndarray)) and element.size == 0 else element for element in row] for row in arr2])    
    arr2 = np.array([[element[0] if isinstance(element, (list, tuple, np.ndarray)) else element for element in row] for row in arr2])
    print 'Finished fixing array, transforming in pandas Data Frame.'

    print arr1, arr2
    label1 = np.array([0]*10000)#tree1.GetEntries())
    label2 = np.array([1]*10000)#tree2.GetEntries())

    arr1 = np.column_stack((arr1,label1))
    arr2 = np.column_stack((arr2,label2))

    arr1 = pd.DataFrame(arr1)
    arr2 = pd.DataFrame(arr2)
    arr1.columns = names
    arr2.columns = names

    arr = pd.concat([arr1, arr2])
    arr = pd.DataFrame(arr)

    Y = arr['labels']
    X = arr.drop('labels', axis=1)
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.4, random_state=0)

#    print X_train, y_train
    clf = RandomForestClassifier(n_estimators=10)
    clf = clf.fit(X_train, y_train)
    score = clf.score(X_test, y_test)
    print 'Score=',score

    print 'Plotting'
    myscatter = pd.plotting.scatter_matrix(X_train, c=y_train, figsize=(15,15), marker='o', hist_kwds={'bins':20}, s=60, alpha=.8, cmap=mglearn.cm3)
    plt.savefig("Scatter_plot.png")

    print 'Finished pandas, starting pickle'
    pickle.dump(arr ,open(outfile , 'wb'))
    
    print 'Done! Pickle file created:\n{}'.format(outfile)

if __name__ == "__main__" :
    print 'ROOTtoPandas Starts'

    print 'Input file: ', infile
    print 'Input file 2: ', infile2
    main(infile, tree1='MonAnaTree', infile2=infile2, tree2='MonAnaTree')

