import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from root_numpy import * # loads all the functions that root_numpy can do

#from IPython.display import display
#from root_numpy import root2array, tree2array
#from root_numpy import testdata

filename1gD = '/eos/user/a/anmrodri/Tevatron/HIP/PG/monopole_single_1gD_500GeV.MonFile.root'
filename2gD = '/eos/user/a/anmrodri/Tevatron/HIP/PG/monopole_single_2gD_500GeV.MonFile.root'

# Convert a TTree in a ROOT file into a NumPy structured array
arr1 = root2array(filename1gD)#, 'MonAnaTree')
#arr2 = root2array(filename2gD)#, 'MonAnaTree')
print "this is 1"
# The TTree name is always optional if there is only one TTree in the file

# Or first get the TTree from the ROOT file
#import ROOT
#rfile = ROOT.TFile(filename)
#intree = rfile.Get('MonAnaTree')

# and convert the TTree into an array
#array = tree2array(intree)
#print "This is 2"

print list_branches(filename1gD, 'MonAnaTree')
#print arr1[1]
#print arr2[1]

warray = arr1['w']
print "This is the 10th entry of the 'w' branch, first component: \n"+str(arr1['w'][10][0])

# if I wanted to point to a number in the array of vectors I would do: warray[entry][firstentry] 

#for iarray in warray:
#    if len(iarray)==0:
#        continue
#    if iarray[0] > 0.5:
#        print iarray[0]
#    else:
#        print "It is smaller"
