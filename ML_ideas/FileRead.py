import ROOT
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split #splits the training from the testing
from sklearn.model_selection import cross_val_score
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import cPickle as pickle
import sys
from root_numpy import * # loads all the functions that root_numpy can do

#from IPython.display import display

filename1gD = '/eos/user/a/anmrodri/Tevatron/HIP/PG/monopole_single_1gD_500GeV.MonFile.root'
filename2gD = '/eos/user/a/anmrodri/Tevatron/HIP/PG/monopole_single_2gD_500GeV.MonFile.root'

# Convert a TTree in a ROOT file into a NumPy structured array
arr1 = root2array(filename1gD) #, 'MonAnaTree')
arr2 = root2array(filename2gD) #, 'MonAnaTree')

columns=list_branches(filename1gD, 'MonAnaTree')

fht_array1 = np.array(arr1['fHT'][:30]) #This limits to only 10 entries
fht_array2 = np.array(arr2['fHT'][:30])

Y=[]
count=0

for fht_i in fht_array1:
    if not fht_i.any():
        fht_array1[count] = -999
#        fht_array1[count] = np.array([999])
    else:
        fht_array1[count] = fht_i[0]
    Y.append(1)
    count+=1
count=0
for fht_i in fht_array2:
    if not fht_i.any():
        fht_array2[count] = -999
#        fht_array1[count] = np.array([999])
    else:
        fht_array2[count] = fht_i[0]
    Y.append(0)
    count+=1

Y = np.array(Y)
X = [fht_array1, fht_array2]

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.4, random_state=0)

print "This is the data array:\n",X_train,"\n This is the second array:\n",y_train

clf = RandomForestClassifier(n_estimators=10)
clf = clf.fit(X_train, y_train)
score = clf.score(X_test, y_test)
print score

#scores = cross_val_score(clf, X, Y, cv=2)
#scores.mean()


# if I wanted to point to a number in the array of vectors I would do: warray[entry][firstentry] 
#for iarray in warray:
#    if len(iarray)==0:
#        continue
#    if iarray[0] > 0.5:
#        print iarray[0]
#    else:
#        print "It is smaller"
