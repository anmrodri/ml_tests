#!/usr/bin/python
import ROOT
from root_numpy import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cPickle as pickle
import mglearn
import sys
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split #splits the training from the testing          
from sklearn.model_selection import cross_val_score

infile = '/eos/user/a/anmrodri/Tevatron/HIP/PG/monopole_single_1gD_500GeV.MonFile.root'
infile2 = '/eos/user/a/anmrodri/Tevatron/HIP/PG/monopole_single_2gD_500GeV.MonFile.root'
tree1 = 'MonAnaTree'
columns = list_branches(infile, tree1)[65:70]
names = columns
selection = ''

def main(infile, tree1, infile2, tree2):

    tree1 = ROOT.TChain(tree1)
    tree1.Add(infile)

    tree2 = ROOT.TChain(tree2)
    tree2.Add(infile2)
    
    print 'Got TTrees, reading into array'
    arr1 = tree2array(tree1, branches=columns, selection=selection)
#    arr2 = tree2array(tree2, branches=columns, selection=selection)

#    names.append('labels')
#    print names

#    print 'Tree2array finished, starting array fixing.'

    arr1 = np.array([[-999 if not element.any() else element for element in row] for row in arr1])
    arr1 = np.array([[element if not element.__len__() else element[0] for element in row] for row in arr1])

#    arr2 = np.array([[-999 if not element.any() else element for element in row] for row in arr2])

#    print 'Finished fixing array, transforming in pandas Data Frame.'

#    print arr1, arr2
#    label1 = np.array([0]*tree1.GetEntries())
#    label2 = np.array([1]*tree2.GetEntries())

#    arr1 = np.column_stack((arr1,label1))
#    arr2 = np.column_stack((arr2,label2))

    arr1 = pd.DataFrame(arr1)
#    arr2 = pd.DataFrame(arr2)
    arr1.columns = names
#    arr2.columns = names

#    arr = pd.concat([arr1, arr2])
#    arr = pd.DataFrame(arr)

    print arr1

if __name__ == "__main__" :
    print 'ROOTtoPandas Starts'

    print 'Input file: ', infile
    print 'Input file 2: ', infile2
    main(infile, tree1='MonAnaTree', infile2=infile2, tree2='MonAnaTree')

